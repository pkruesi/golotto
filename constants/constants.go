package constants

const (
	PORT        = ":8088"
	STATICFILES = "static"
	INDEX       = STATICFILES + "/index.html"
	LOGFILE     = "log/golotto.log"
)
