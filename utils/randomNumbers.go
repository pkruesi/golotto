package utils

import (
	"math/rand"
	"sort"
	"time"
)

func UnicRandomNumbersFromTo(amountOfNumbers int, min int, max int) []int {
	rand.Seed(time.Now().UnixNano())

	lNumbers := make([]int, amountOfNumbers)
	//initialize the slice so that we can also have zero as random number
	for i := range lNumbers {
		lNumbers[i] = -1
	}

	var n int
	var exists bool
	for i := 0; i < len(lNumbers); i++ {

		for {
			n = rand.Intn(max-min) + min
			exists = false
			for k := 0; k < len(lNumbers); k++ {
				if lNumbers[k] == n {
					exists = true
					break
				}
			}
			if exists != true {
				lNumbers[i] = n
				break
			}

			if exists != true {
				lNumbers[i] = n
				break
			}

		}
	}
	sort.Ints(lNumbers)
	return lNumbers

}

func UnicRandomNumbers(amountOfNumbers int, max int) []int {
	rand.Seed(time.Now().UnixNano())

	lNumbers := make([]int, amountOfNumbers)
	//initialize the slice so that we can also have zero as random number
	for i := range lNumbers {
		lNumbers[i] = -1
	}

	var n int
	var exists bool
	for i := 0; i < len(lNumbers); i++ {

		for {
			n = rand.Intn(max) + 1
			exists = false
			for k := 0; k < len(lNumbers); k++ {
				if lNumbers[k] == n {
					exists = true
					break
				}
			}
			if exists != true {
				lNumbers[i] = n
				break
			}

			if exists != true {
				lNumbers[i] = n
				break
			}

		}
	}
	sort.Ints(lNumbers)
	return lNumbers

}
