package main

import (
	"net/http"

	"github.com/gorilla/mux"

	"pkruesi/golotto/constants"
	"pkruesi/golotto/controllers"
	"pkruesi/golotto/utils"
)

// Entry point from golang application
func main() {

	logger := utils.GetMyLogger()
	logger.Printf("start golotto: Port [%v]\n", constants.PORT)

	// create my router
	rtr := mux.NewRouter()
	// Router from / url and redirect to the root handler
	rtr.HandleFunc("/", controllers.RootHandler)

	// Router of /swisslotto url
	rtr.HandleFunc("/swisslotto", controllers.SwissLotto)

	// Router of /euromillions
	rtr.HandleFunc("/euromillions", controllers.EuroMillions)

	// Define the web root path for css, js and any asset used on the HTML templates
	rtr.PathPrefix("/static/").Handler(http.StripPrefix("/"+constants.STATICFILES+"/", http.FileServer(http.Dir("./"+constants.STATICFILES))))

	// Start HTTP handle
	http.Handle("/", rtr)

	// Application will listen port <nnnn>, where nnnn is configured on constants package
	logger.Println(http.ListenAndServe(constants.PORT, nil))
}
