package utils

import (
	"log"
	"os"
	"sync"

	"pkruesi/golotto/constants"
)

type howsLogger struct {
	filename string
	*log.Logger
}

var mylogger *howsLogger
var once sync.Once

// start logger and do
func GetMyLogger() *howsLogger {
	once.Do(func() {
		mylogger = createMyLogger(constants.LOGFILE)
	})

	return mylogger
}

func createMyLogger(logfilename string) *howsLogger {
	logfile, _ := os.OpenFile(logfilename, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0660)

	return &howsLogger{
		filename: logfilename,
		Logger:   log.New(logfile, "golotto ", log.Ldate|log.Ltime|log.Lshortfile),
	}
}
