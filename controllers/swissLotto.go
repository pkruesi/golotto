package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"

	"pkruesi/golotto/utils"
)

type swissLottoNumbers struct {
	Numbers []int `json:"numbers"`
	Lucky   []int `json:"lucky"`
}

// SwissLotto, shows all entries
func SwissLotto(w http.ResponseWriter, r *http.Request) {
	logger.Printf("%s %s\n", r.RequestURI, r.RemoteAddr)
	sln := swissLottoNumbers{Numbers: utils.UnicRandomNumbers(6, 42), Lucky: utils.UnicRandomNumbers(1, 6)}
	slnJson, err := json.Marshal(sln)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")

	fmt.Fprintf(w, "%s", slnJson)
}
