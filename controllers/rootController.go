package controllers

import (
	"net/http"
)

// Root handler delivers /index.html
func RootHandler(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, indexHTML)
	logger.Printf("%s %s %d\n", r.RequestURI, r.RemoteAddr, http.StatusOK)
}
