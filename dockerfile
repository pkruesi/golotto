FROM scratch
MAINTAINER Nobody
ADD ./golotto ./golotto
ADD ./log /log
ADD ./static /static
ENV PORT 8088
EXPOSE 8088
ENTRYPOINT ["/golotto"]
