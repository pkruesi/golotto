# A small test app
* generates some random lotto numbers
  * ```/swisslotto```
    * random lotto numbers for swisslotto
  * ```/euromillions``` 
    * random lotto numbers for euromillions

# go build
```bash
go build -o golotto golotto.go
```

# Docker build
```bash
docker build -t golotto:$(date +%F) .
```

# Docker run
```bash
docker run -d -v /var/spool/golotto/:/data -p 8088:8088 golotto:$(date +%F)

# example
docker run -d -v /var/spool/golotto/:/data -p 8088:8088 golotto:2018-08-24

# with auto restart after reboot
docker run --restart=unless-stopped -d -v /var/spool/golotto/:/data -p 8088:8088 golotto:2018-08-24
```
